﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Models
{
    public class Comment
    {
        public int ID { get; set; }
        public string Text { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime DeletedDate { get; set; }
        public int? DiscountId { get; set; }
        public virtual Discount Discount { get; set; }
        public int? CompanyId { get; set; }
        public virtual Company Company { get; set; }
        public int? UserId { get; set; }
        public virtual User User { get; set; }
    }
}
