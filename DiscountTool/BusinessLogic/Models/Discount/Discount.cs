﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Models
{
    public class Discount
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Preview { get; set; }
        public int Percent { get; set; }
        public string Details { get; set; }
        public bool IsActive { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public int? CategoryId { get; set; }
        public virtual Category Category { get; set; }
        public int? CompanyId { get; set; }
        public virtual Company Company { get; set; }
    }
}
