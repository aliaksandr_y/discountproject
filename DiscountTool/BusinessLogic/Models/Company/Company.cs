﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Models
{
    public class Company
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string WebSite { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public ICollection<Discount> Discounts { get; set; }

        public ICollection<Comment> Comments { get; set; }
    }
}
