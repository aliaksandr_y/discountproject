﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services.Role
{
    public static class RoleHelper
    {
        public static List<int> GetRoleNumbers(List<string> roles)
        {
            List<int> roleNumbers = new List<int>();
            foreach (var role in roles)
            {
                roleNumbers.Add(GetRoleNumber(role));
            }
            return roleNumbers;
        }

        public static int GetRoleNumber(string role)
        {
            switch (role)
            {
                case RoleNames.AdministratorRole: return (int)Roles.Administrator;
                case RoleNames.ModeratorRole: return (int)Roles.Moderator;
                default: return (int)Roles.Employee;
            }
        }
    }
}
