﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public static class RoleNames
    {
        public const string AdministratorRole = "Administrator";
        public const string ModeratorRole = "Moderator";
        public const string EmployeeRole = "Employee";
    }
    public enum Roles
    {
        Employee = 1,
        Moderator = 2,
        Administrator = 3
    }
}
