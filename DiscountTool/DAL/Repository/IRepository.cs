﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IRepository<TEntity> where TEntity : class 
    {
        int TotalCount();

        IEnumerable<TEntity> GetAll();

        void GetById(int id);

        IEnumerable<TEntity> GetAll(IEnumerable<string> includes);

        void Add(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);
    }
}
