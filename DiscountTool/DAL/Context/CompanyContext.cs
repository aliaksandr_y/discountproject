﻿using BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Context
{
    public class CompanyContext : DbContext
    {
        public CompanyContext() : base("DefaultConnection")
    { }
        public DbSet<Company> Companies { get; set; }
    }
}
