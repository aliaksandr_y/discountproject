﻿using BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Context
{
    public class DiscountContext : DbContext
    {
        public DiscountContext() : base("DefaultConnection")
        { }
        public DbSet<Discount> Discounts { get; set; }
    }
}
