﻿using BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Context
{
    public class CategoryContext : DbContext
    {
        public CategoryContext() : base("DefaultConnection")
    { }
        public DbSet<Category> Categories { get; set; }
    }
}
