﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('homeCtrl', homeCtrl);

    homeCtrl.$inject = ['$scope']; 

    function homeCtrl($scope) {
        $scope.title = 'homeCtrl';

        activate();

        function activate() { }
    }
})();
